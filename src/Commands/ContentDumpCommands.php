<?php

namespace Drupal\content_dump\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drush\Commands\DrushCommands;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Drupal\Core\Batch\BatchBuilder;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ContentDumpCommands extends DrushCommands {

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Valid formats.
   *
   * @var array
   */
  protected $formats;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   Serializer.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param array $serializer_formats
   *   Array of string for the valid formats.
   */
  public function __construct(FileSystemInterface $file_system, SerializerInterface $serializer, EntityTypeManagerInterface $entity_type_manager, array $serializer_formats) {
    $this->entityTypeManager = $entity_type_manager;
    $this->serializer = $serializer;
    $this->fileSystem = $file_system;
    $this->formats = $serializer_formats;
    parent::__construct();
  }

  /**
   * Dump all entity content to files.
   *
   * @option types A comma separated list of entity types to dump.
   * @option format The format to serialize the data in.  Defaults to json.
   *
   * @command content_dump:all
   * @aliases cda
   */
  public function dump($options = ['types' => '', 'format' => 'json']) {
    // Validate the options.
    if (!in_array($options['format'], $this->formats)) {
      throw new \Exception(dt('Invalid format specified.  Valid formats are: @formats', ['@formats' => implode(', ', $this->formats)]));
    }

    if (!empty($options['types'])) {
      $types = explode(',', $options['types']);
      $entity_types = $this->entityTypeManager->getDefinitions();
      // Get ID of each entity type in an array.
      $entity_type_ids = array_map(function ($entity_type) {
        return $entity_type->id();
      }, $entity_types);
      // Check the types specified are valid.
      $invalid_types = array_diff($types, $entity_type_ids);
      if (!empty($invalid_types)) {
        throw new \Exception(dt('Invalid entity types specified: @types', ['@types' => implode(', ', $invalid_types)]));
      }
      $entity_types = array_filter($entity_types, function ($entity_type) use ($types) {
        return in_array($entity_type->id(), $types);
      });
    }
    else {
      $entity_types = $this->entityTypeManager->getDefinitions();
    }

    // Skip some entity types.
    $skip = ['user'];

    $directory = "public://content_dump";
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

    $batch_builder = (new BatchBuilder())
      ->setTitle('Dumping content')
      ->setInitMessage('Dumping content')
      ->setErrorMessage('An error occurred while dumping content')
      ->setProgressive(TRUE);

    foreach ($entity_types as $entity_type) {
      if (in_array($entity_type->id(), $skip)) {
        continue;
      }
      $entity_directory = $directory . '/' . $entity_type->id();
      [$operation, $arguments] = $this->makeDumpBatch($entity_directory, $entity_type, $options['format']);
      $batch_builder->addOperation($operation, $arguments);
    }

    batch_set($batch_builder->toArray());
    drush_backend_batch_process();
  }

  /**
   * Build a batch array to dump an entity type.
   *
   * @param string $entity_directory
   *   Path to a directory to write files to.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type to dump.
   * @param string $format
   *   The format to dump the files in.
   *
   * @return array
   *   A batch array.
   */
  public function makeDumpBatch(string $entity_directory, EntityTypeInterface $entity_type, string $format) {
    $this->fileSystem->prepareDirectory($entity_directory, FileSystemInterface::CREATE_DIRECTORY);
    $entity_storage = $this->entityTypeManager->getStorage($entity_type->id());
    $entities = $entity_storage->getQuery()->execute();
    $this->logger()->notice(dt('Queuing @count @entity_type entities', [
      '@count' => count($entities),
      '@entity_type' => $entity_type->id(),
    ]));
    return [
      [self::class, 'batchDump'],
      [$entity_directory, $entity_type, $format, $entities],
    ];

  }

  /**
   * Static batch operation to dump entities.
   *
   * @param string $entity_directory
   *   Path to a directory to write files to.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type to dump.
   * @param string $format
   *   The format to dump the files in.
   * @param array $entity_ids
   *   The entity ids to dump.
   * @param array|\ArrayAccess $context
   *   Batch context.
   */
  public static function batchDump(string $entity_directory, EntityTypeInterface $entity_type, string $format, array $entity_ids, &$context) {
    $serializer = \Drupal::service('serializer');
    $fileSystem = \Drupal::service('file_system');
    $logger = \Drupal::service('logger.factory')->get('content_dump');

    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($entity_ids);
      $context['sandbox']['entity_ids'] = $entity_ids;
    }

    $count = min(100, count($context['sandbox']['entity_ids']));
    $batch_entities = array_splice($context['sandbox']['entity_ids'], 0, $count);
    $context['sandbox']['progress'] += $count;
    $entities = \Drupal::entityTypeManager()
      ->getStorage($entity_type->id())
      ->loadMultiple($batch_entities);
    foreach ($entities as $entity) {
      try {
        $serialized_entity = $serializer->serialize($entity, $format, ['plugin_id' => 'entity']);
        $file_name = $entity_type->id() . '-' . $entity->id() . '.' . $format;
        $file_path = $entity_directory . '/' . $file_name;
        $fileSystem->saveData($serialized_entity, $file_path);
      }
      catch (\Exception $exception) {
        $logger
          ->error(dt('Entity @entity_type @entity_id could not be dumped due to @reason', [
            '@entity_type' => $entity_type->id(),
            '@entity_id' => $entity->id(),
            '@reason' => $exception->getMessage(),
          ]));
      }
    }
    $context['message'] = 'Dumped ' . $context['sandbox']['progress'] . ' ' . $entity_type->id() . ' entities';

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

}
